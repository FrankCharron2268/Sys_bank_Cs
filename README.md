To Download: Click the cloud-looking button on the right of the screen

To launch project:
- Download or Pull Project.
- Open SystemBanquier_Build.
- Launch SystemBanquier.exe.

To view solution:
- Download or Pull Project.
- Open SystemBanquier_Solution -> SystemBanquier. 
- Open SystemBanquier.sln in your IDE or double click it.