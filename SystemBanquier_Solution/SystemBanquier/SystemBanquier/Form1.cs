﻿//Written By Frank
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SystemBanquier
{ // OPEN REGIONS TO VIEW CODE
    public partial class Form1 : Form
    {
        #region Listes & declarations

        public List<Directeur> listDirecteurs;
        public List<Banques> listBanques;

        public List<Agent> listAgent;
        public List<Client> listClients;
        public List<Comptes> listCompteClient;
        public List<Succursales> listSuccursale;
        public List<string> listnomsucc;
        public List<CompteRem> listComptesRem;
        public List<CompteNonRem> listComptesNonRem;

        #endregion Listes & declarations

        public Form1()
        {
            #region initialisations de listes, classes par defauts & placements des elements par defauts dans les controls

            listComptesRem = new List<CompteRem>();
            listComptesNonRem = new List<CompteNonRem>();
            listDirecteurs = new List<Directeur>();
            listBanques = new List<Banques>();
            listSuccursale = new List<Succursales>();
            listAgent = new List<Agent>();
            listCompteClient = new List<Comptes>();
            listClients = new List<Client>();
            listnomsucc = new List<string>();
            InitializeComponent();
            Directeur iniDir = new Directeur("iniDir", 250000, "Big", "Boss");
            Banques iniBank = new Banques("Banque de Laval", "888 lucky street", iniDir, 5000000000, listSuccursale, listnomsucc);
            Succursales iniSucc = new Succursales("iniSucc", "Succheure", listAgent, iniBank.NOMBANQUE, "456 Tateto street", iniBank.DIRECTEUR, 500000, listSuccursale, listnomsucc);
            Personnes iniPers = new Personnes("No", "Where");
            Agent iniAgent = new Agent(dtpDateEmp.Value, listClients, iniSucc.IDSUCC, "iniAgent", 45000, "John", "Doe", "777 lucky street");
            Client iniClient = new Client("iniClient", listCompteClient, iniAgent.empID(), iniAgent.toString(), "Le", "Client");
            //Comptes iniComptes = new Comptes("iniComptes", 500000, iniClient.IdClient());
            //CompteNonRem iniCNR = new CompteNonRem("iniCNR", 75, iniClient.IdClient());
            //CompteRem iniCR = new CompteRem(0.10, "iniCR", -75, iniClient.IdClient());
            double resultTransaction;
            listDirecteurs.Add(iniDir);
            listBanques.Add(iniBank);
            listSuccursale.Add(iniSucc);
            listAgent.Add(iniAgent);
            listClients.Add(iniClient);
            // listCompteClient.Add(iniCNR);

            cbAgentClient.Items.Add(iniAgent.toString());
            lbDir.Items.Add(iniDir.toString());///dir
            cbDirecteurBanque.Items.Add(iniDir.toString());///Bank
            lbBanque.Items.Add(iniBank.NOMBANQUE);
            cbBanqueSucc.Items.Add(iniBank.NOMBANQUE);///Succ
            // lbSucc.Items.Add(iniSucc.NOMSUCC);
            //// cbSuccEmp.Items.Add(iniSucc.NOMSUCC);///EMP
            lbEmp.Items.Add(iniAgent.toString());
            cbSuccursaleClient.Items.Add(iniSucc.NOMSUCC);///Client
            lbClients.Items.Add(iniClient.toString());
        }

        #endregion initialisations de listes, classes par defauts & placements des elements par defauts dans les controls

        #region button controls

        private void butDepotOp_Click_1(object sender, EventArgs e)
        {
            Client clientx = null;
            clientx = loginClient(clientx);
            double depot = 0;
            depot = Convert.ToDouble(txtMontantOp.Text);
            clientx.LISTCOMPTESCLIENTS[lbInfoClient.SelectedIndex].depositFunds(depot);
        } //WORKS

        private void butRetraitOp_Click_1(object sender, EventArgs e)
        {
            Client clientx = null;
            clientx = loginClient(clientx);
            double retrait = 0;
            retrait = Convert.ToDouble(txtMontantOp.Text);
            clientx.LISTCOMPTESCLIENTS[lbInfoClient.SelectedIndex].withdrawFunds(retrait);
        }  //WORKS

        private void butCreerDir_Click(object sender, EventArgs e)
        {
            Directeur emptyDir = new Directeur();
            string ID = emptyDir.empID();
            Directeur dir1 = new Directeur(ID, Convert.ToDouble(txtSalaireDir.Text), txtNomDir.Text, txtPrenomDir.Text);
            listDirecteurs.Add(dir1);
            lbDir.Items.Add(dir1.toString());
            cbDirecteurBanque.Items.Add(dir1.toString());
        }//Works

        private void butEffDir_Click(object sender, EventArgs e)
        {
            cbDirecteurBanque.Items.RemoveAt(lbDir.SelectedIndex);
            listDirecteurs.RemoveAt(lbDir.SelectedIndex);
            lbDir.Items.RemoveAt(lbDir.SelectedIndex);
        }//Works

        private void butModDir_Click(object sender, EventArgs e) ///to change
        {
            double salaire = Convert.ToDouble(TXTSALAIREDIR.Text);
            listDirecteurs[lbDir.SelectedIndex].ChangeRevenuDir(salaire);
        }//works

        private void butCreerBanque_Click(object sender, EventArgs e)
        {
            double capitalGlobal = 0;
            Banques ban1 = new Banques(txtNomBanque.Text, txtAdresseBanque.Text, listDirecteurs[cbDirecteurBanque.SelectedIndex], capitalGlobal, listSuccursale, listnomsucc);
            listBanques.Add(ban1);
            lbBanque.Items.Add(ban1.toString());
            cbBanqueSucc.Items.Add(ban1.toString());
        }///Works

        private void EffBanque_Click(object sender, EventArgs e)
        {
            cbBanqueSucc.Items.RemoveAt(lbBanque.SelectedIndex);
            listBanques.RemoveAt(lbBanque.SelectedIndex);
            lbBanque.Items.RemoveAt(lbBanque.SelectedIndex);
        }//Works

        private void ModBanque_Click(object sender, EventArgs e)
        {
            string adresse = txtAdresseBanque.Text;
            listBanques[lbBanque.SelectedIndex].ModBank(adresse);
        }//works

        private void CreerSucc_Click(object sender, EventArgs e)///Works
        {
            double capitalGlobal = 0;
            Succursales succ00 = new Succursales();
            string ID = succ00.IDSUCC;

            Succursales succ1 = new Succursales(ID, txtNomSucc.Text, listAgent, listBanques[cbBanqueSucc.SelectedIndex].NOMBANQUE, txtAdresseSucc.Text, listBanques[cbBanqueSucc.SelectedIndex].DIRECTEUR, capitalGlobal, listSuccursale, listnomsucc);

            Banques x = listBanques[cbBanqueSucc.SelectedIndex];
            x.LISTSUCCURSALE.Add(succ1);
            x.LISTNOMSUCC.Add(succ1.IDSUCC);
            lbSucc.Items.Add(succ1.NOMSUCC);
            cbSuccEmp.Items.Add(succ1.NOMSUCC);
            cbSuccursaleClient.Items.Add(succ1.toString());
        }///Works

        private void EffSucc_Click(object sender, EventArgs e)
        {
            cbSuccursaleClient.Items.RemoveAt(lbSucc.SelectedIndex);
            cbSuccEmp.Items.RemoveAt(lbSucc.SelectedIndex);
            listSuccursale.RemoveAt(lbSucc.SelectedIndex);
            lbSucc.Items.RemoveAt(lbSucc.SelectedIndex);
        }//works

        private void ModSucc_Click(object sender, EventArgs e)
        {
            string adresse2 = txtAdresseSucc.Text;
            listSuccursale[lbSucc.SelectedIndex].modSucc(adresse2);
        }//works

        private void CreerEmp_Click(object sender, EventArgs e)
        {
            createEMP();
        }///Works

        private void EffEmp_Click(object sender, EventArgs e)
        {
            listAgent.RemoveAt(lbEmp.SelectedIndex);
            lbEmp.Items.RemoveAt(lbEmp.SelectedIndex);
        }//TODO

        private void butModEmp_Click_1(object sender, EventArgs e)
        {
        }  //TODO

        private void butTransfEmp_Click_1(object sender, EventArgs e)
        {
        }  //TODO

        private void butTransfEmp_Click(object sender, EventArgs e)
        {
        }  //TODO

        private void butCreerClient_Click(object sender, EventArgs e)
        {
            createClient();
        }//works

        private void butCreerCompte_Click(object sender, EventArgs e)
        {
            if (radbutCompteRem.Checked)
            {
                sendToCR();
                return;
            }
            else if (radbutCompteNonRem.Checked)
            {
                sendToCNR();
                return;
            }
        } //works

        private void showInfoBanque_Click(object sender, EventArgs e)
        {
            lbInfoBank.Items.Add(listBanques[lbBanque.SelectedIndex].NOMBANQUE);
            lbInfoBank.Items.Add(listBanques[lbBanque.SelectedIndex].ADRESSE);
            lbInfoBank.Items.Add(listBanques[lbBanque.SelectedIndex].DIRECTEUR.toString());
            lbInfoBank.Items.Add(listBanques[lbBanque.SelectedIndex].CAPITAL);
            double capitalTemp = 0;
            foreach (Succursales element in listBanques[lbBanque.SelectedIndex].LISTSUCCURSALE)
            {
                lbInfoBank.Items.Add(element.NOMSUCC);
                var temp = element.LISTAGENT;
                foreach (Agent element2 in temp)
                {
                    var temp2 = element2.LISTCLIENTS;
                    foreach (Client element3 in temp2)
                    {
                        var temp3 = element3.LISTCOMPTESCLIENTS;
                        foreach (CompteNonRem element4 in temp3)
                        {
                            listBanques[lbBanque.SelectedIndex].addToCapital(capitalTemp);
                        }
                        foreach (CompteNonRem element5 in temp3)
                        {
                            listBanques[lbBanque.SelectedIndex].addToCapital(capitalTemp);
                        }
                    }
                }
            }
        }//works

        private void butConnect_Click(object sender, EventArgs e)
        {
            listComptesNonRem.Clear();
            listComptesRem.Clear();
            connectClient();
            Client clientx = null;
            clientx = loginClient(clientx);
            lbInfoClient1.Items.Add("ID: " + clientx.IDCLIENT);
            lbInfoClient1.Items.Add("Nom: " + clientx.NOM);
            lbInfoClient1.Items.Add("Prenom: " + clientx.PRENOM);
            lbInfoClient1.Items.Add("");
            lbInfoClient1.Items.Add("Info sur votre Agent");
            lbInfoClient1.Items.Add("ID: " + clientx.IDAGENT);
            lbInfoClient1.Items.Add("Nom: " + clientx.NOMAGENT);
            butCreerCompte.Enabled = true;
        }//WORKS

        private void butDeconnect_Click(object sender, EventArgs e)
        {
            lbInfoClient.Items.Clear();

            butCreerCompte.Enabled = false;
            butDepotOp.Enabled = false;
            butRetraitOp.Enabled = false;
        }//WORKS

        private void butShowInfoAcc_Click(object sender, EventArgs e)
        {
            List<CompteNonRem> listCNRtemp = new List<CompteNonRem>();
            List<CompteRem> listCRtemp = new List<CompteRem>();

            lbInfoClient.Items.Clear();

            Client clientx = null;
            clientx = loginClient(clientx);
            butDepotOp.Enabled = false;
            butRetraitOp.Enabled = false;
            foreach (object element in clientx.LISTCOMPTESCLIENTS)
            {
                if (element is CompteRem)
                {
                    CompteRem CRTemp = (CompteRem)element;
                    listCRtemp.Add(CRTemp);
                    foreach (CompteRem element2 in listCRtemp)
                    {
                        lbInfoClient.Items.Add(element2.infoCompte());
                    }
                }
                else if (element is CompteNonRem)
                {
                    CompteNonRem CNRTemp = (CompteNonRem)element;
                    listCNRtemp.Add(CNRTemp);

                    foreach (CompteNonRem element3 in listCNRtemp)
                    {
                        lbInfoClient.Items.Add(element3.infoCompte());
                    }
                }
            }

            if (listCRtemp.Count > 0)
            {
                for (int i = 0; i < listCRtemp.Count; i++)
                {
                    listCRtemp.RemoveAt(i);
                }
            }
            if (listCNRtemp.Count > 0)
            {
                for (int j = 0; j < listCNRtemp.Count; j++)
                {
                    listCNRtemp.RemoveAt(j);
                }
            }
        }//TODO

        #endregion button controls

        #region ChangedEvents

        private void lbDir_SelectedIndexChanged(object sender, EventArgs e)
        {
            butModDir.Enabled = true;
        }

        private void lbBanque_SelectedIndexChanged(object sender, EventArgs e)
        {
            showInfoBanque.Enabled = true;
        }

        private void cbDirecteurBanque_SelectedIndexChanged(object sender, EventArgs e)
        {
            butCreerBanque.Enabled = true;
        }

        private void cbBanqueSucc_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreerSucc.Enabled = true;
        }

        private void cbSuccEmp_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreerEmp.Enabled = true;
        }

        private void cbAgentClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            butCreerClient.Enabled = true;
        }

        private void txtIDClientCompte_TextChanged(object sender, EventArgs e)
        {
        }

        private void lbInfoClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            butRetraitOp.Enabled = true;
            butDepotOp.Enabled = true;
        }

        public TextBox TXTSALAIREDIR
        {
            get { return this.txtSalaireDir; }
        }

        public ComboBox CBSUCCEMP
        {
            get { return this.cbSuccEmp; }
            set { this.cbSuccEmp = value; }
        }

        public ListBox LBEMP
        {
            get { return this.lbEmp; }
            set { this.lbEmp = value; }
        }

        #endregion ChangedEvents

        #region functions

        private Client loginClient(Client clientx)
        {
            foreach (Client element in listClients)
            {
                if (element.IDCLIENT == txtIDClientCompte.Text)
                {
                    clientx = element;
                }
            }
            return clientx;
        } //works

        public void connectClient()
        {
            lbInfoClient.Items.Clear();
            lbInfoClient1.Items.Clear();
            Client clientx = null;
            clientx = loginClient(clientx);
        }//WORKS

        public void sendToCR()
        {
            CompteRem CR00 = new CompteRem();
            string ID = CR00.compteID();

            Client clientx = null;
            clientx = loginClient(clientx);
            CompteRem CR01 = new CompteRem(0.10, ID, Convert.ToDouble(txtDepotCompte.Text), clientx.IdClient());

            clientx.addCR(clientx, CR01);
            lbInfoClient1.Items.Add(CR01.compteID());
        }//works

        public void createEMP()
        {
            Agent A00 = new Agent();
            string ID = A00.empID();
            Agent A01 = new Agent(dtpDateEmp.Value, listClients, listSuccursale[cbSuccEmp.SelectedIndex].IDSUCC, ID, Convert.ToDouble(txtSalaireEmp.Text), txtNomEmp.Text, txtPrenomEmp.Text, txtAdressEmp.Text);
            lbEmp.Items.Add(A01.toString());
            cbAgentClient.Items.Add(A01.toString());
            listSuccursale[cbSuccEmp.SelectedIndex].addEmp(A01);
        }//works

        private void createClient()
        {  /// Comment faire mieu ?? Comment faire comme jai fait dans pour les comptes.
           ///n'y arrive pas a cause que je dois envoyer le client dans le listbox
            Client CLI00 = new Client();
            string ID = CLI00.IdClient();
            Client CLI01 = new Client(txtIdClient.Text, listCompteClient, listAgent[cbAgentClient.SelectedIndex].empID(), listAgent[cbAgentClient.SelectedIndex].toString(), txtNomClient.Text, txtPrenomClient.Text);
            lbClients.Items.Add(CLI01.toString());
            listAgent[cbAgentClient.SelectedIndex].addClient(CLI01);
        }//works

        public void sendToCNR()
        {
            CompteNonRem CNR00 = new CompteNonRem();
            string ID = CNR00.compteID();
            Client clientx = null;
            clientx = loginClient(clientx);

            CompteNonRem CNR01 = new CompteNonRem(ID, Convert.ToDouble(txtDepotCompte.Text), clientx.IdClient());
            clientx.addCNR(clientx, CNR01);
            lbInfoClient1.Items.Add(CNR01.compteID());
        }//works

        #endregion functions
    }
}