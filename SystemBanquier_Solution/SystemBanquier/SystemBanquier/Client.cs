﻿//Written By Frank
using System.Collections.Generic;

namespace SystemBanquier
{
    public class Client : Personnes
    {
        private string idClient;
        private string idAgent { get; set; }
        private string nomAgent { get; set; }
        private List<Comptes> listCompteClient { get; set; }
        private static int cnt = 0;

        public Client()
        {
        }

        public Client(string idClient, List<Comptes> listCompteClient, string idAgent, string nomAgent,
            string Nom, string Prenom)
            : base(Nom, Prenom)
        {
            this.idClient = idClient;
            this.listCompteClient = listCompteClient;
            this.idAgent = idAgent;
            this.nomAgent = nomAgent;
            cnt++;
        }

        public string toString()
        {
            string result = "";
            result += Nom + Prenom;
            return result;
        }

        public string IdClient() ///update
        {
            string ID = "CLI" + cnt;
            return ID;
        }

        public string IDAGENT
        {
            get { return idAgent; }
        }
        public string NOMAGENT
        {
            get { return nomAgent; }
        }
        public string IDCLIENT
        {
            get { return this.idClient; }
        }

        public List<Comptes> LISTCOMPTESCLIENTS
        {
            get { return this.listCompteClient; }
        }

        public void addCNR(Client x, CompteNonRem y)
        {
            x.LISTCOMPTESCLIENTS.Add(y);
        }

        public void addCR(Client x, CompteRem y)
        {
            x.LISTCOMPTESCLIENTS.Add(y);
        }
    }
}