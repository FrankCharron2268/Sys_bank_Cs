﻿//Written By Frank

using System.Windows.Forms;
namespace SystemBanquier
  
{
    public class CompteRem : Comptes
    {
        private double tauxInt { get; set; }

        public CompteRem()
        {
        }

        public CompteRem(double TauxInt, string CompteId, double Solde, string Idclient)
            : base(CompteId, Solde, Idclient)
        {
            this.compteId = compteId;
            this.tauxInt = 0.05;
        }

        public string compteID()
        {
            string ID = "CR" + cnt;
            return ID;
        }

        public string infoCompte()
        {
            string info = "";
            info += "No de Compte: " + compteId + " Solde: " + solde;
            return info;
        }

       public override void depositFunds(double x)
        {
            solde += x + (x * tauxInt);
        }

        public override void withdrawFunds(double x)
        {
            double checkSolde = solde - x;
            if (checkSolde < 0)
            {

                MessageBox.Show("Fonds Insuffisants votre compte ne permet pas le decouvert");
                return;
            }
            else
            {

                solde -= x ;
            }

        }
    }
}