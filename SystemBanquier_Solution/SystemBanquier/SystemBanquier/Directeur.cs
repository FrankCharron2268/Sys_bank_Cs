﻿//Written By Frank
namespace SystemBanquier
{
    public class Directeur : Personnes
    {
        private string empId { get; set; }
        private double revenu;
        private static int cnt;

        public Directeur(string empId, double revenu, string Nom, string Prenom)
            : base(Nom, Prenom)
        {
            this.Nom = Nom;
            this.empId = empId;
            this.revenu = revenu;
            cnt++;
        }

        public Directeur()
        {
        }

        public string toString()
        {
            string result = "";
            result += Nom;
            result += "  ";
            result += Prenom;

            return result;
        }

        public double REVENU
        {
            get { return this.revenu; }
        }

        public virtual string empID()
        {
            string ID = "EMP" + cnt;
            return ID;
        }

        public void ChangeRevenuDir(double x)
        {
            revenu = x;
        }
    }
}