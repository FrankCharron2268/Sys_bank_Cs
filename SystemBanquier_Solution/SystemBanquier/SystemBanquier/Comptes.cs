﻿//Written By Frank
namespace SystemBanquier
{
    public class Comptes
    {
        protected string compteId { get; set; }
        protected double solde;
        private string idclient { get; set; }
        protected static int cnt;

        public Comptes()
        {
        }

        public Comptes(string CompteId, double Solde, string IdClient)
        {
            this.compteId = CompteId;
            this.solde = Solde;
            this.idclient = IdClient;
            cnt++;
        }

        public double SOLDE
        {
            get { return solde; }
        }
        public virtual void depositFunds(double x)
        {
            solde += x;
        }

        public virtual void withdrawFunds(double x)
        {
            solde -= x;
        }

        public double showSolde()
        {
            return solde;
        }
    }
}