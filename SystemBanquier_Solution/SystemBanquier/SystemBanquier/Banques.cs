﻿//Written By Frank
using System.Collections.Generic;

namespace SystemBanquier
{
    public class Banques
    {
        private string nomBanque, adresse;
        private Directeur directeur;
        private double capitalGlobal;
        protected List<Succursales> ListSuccursale;
        private List<string> Listnomsucc;
        private static int cnt = 0;

        public Banques() ///update
        {
        }

        public Banques(string nomBanque, string adresse, Directeur directeur, double capitalGlobal, List<Succursales> listSuccursale, List<string> listnomsucc)
        {
            this.nomBanque = nomBanque;
            this.adresse = adresse;
            this.directeur = directeur;
            this.capitalGlobal = capitalGlobal;
            this.ListSuccursale = listSuccursale;
            this.Listnomsucc = listnomsucc;
            cnt++;
        }

        public virtual string toString()
        {
            string result = nomBanque;
            return result;
        }

        public string infoToString()
        {
            string result = "";
            result += "Nom: " + nomBanque + "\n";
            result += "\n";
            result += "addresse: " + adresse + "\n";
            result += "\n";
            result += "Directeur: " + DIRECTEUR.toString() + "\n";
            result += "\n";
            result += "Capital : " + capitalGlobal + "\n";
            result += "\n";
            result += "Succursales: " + Listnomsucc + "\n";

            return result;
        }
        public void addToCapital(double x)
        {
            capitalGlobal += x;
        }

        public string NOMBANQUE
        {
            get { return this.nomBanque; }
        }

        public virtual string ADRESSE
        {
            get { return this.adresse; }
            set { this.adresse = value; }
        }

        public Directeur DIRECTEUR
        {
            get { return this.directeur; }
        }

        public List<Succursales> LISTSUCCURSALE
        {
            get { return this.ListSuccursale; }
        }

        public List<string> LISTNOMSUCC
        {
            get { return this.Listnomsucc; }
        }

        public double CAPITAL
        {
            get { return this.capitalGlobal; }
            
        }

        public void ModBank(string x)
        {
            adresse = x;
        }
    }
}