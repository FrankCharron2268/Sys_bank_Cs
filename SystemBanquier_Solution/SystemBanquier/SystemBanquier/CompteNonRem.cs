﻿//Written By Frank
using System;
using System.Windows.Forms;

namespace SystemBanquier
{
    public class CompteNonRem : Comptes
    {
        private string compteId { get; set; }

        private const double decouvertMax = -300;
        private const double intDecouvert = 0.05;

        public CompteNonRem()
        {
        }

        public CompteNonRem(string compteId, double solde,
        string idclient) : base(compteId, solde, idclient)
        {
            this.compteId = compteId;
        }

        public string compteID() ///update
        {
            string ID = "CNR" + cnt;
            return ID;
        }

        public string infoCompte()
        {
            string info = "";
            info += "No de Compte: " + compteId + " Solde: " + solde;
            return info;
        }

        public override void withdrawFunds(double x)
        {
            double temp = solde - x;
            double temp2 = temp * -1;

            if (base.solde < 0)
            {
                double checkMarge = solde - x + (x * intDecouvert);
                if (checkMarge < decouvertMax)
                {
                    MessageBox.Show("fonds insuffisants");
                    return;
                }
                else
                {
                    solde -= x + (x * intDecouvert);
                    return;
                }
            }
            else if (temp < 0)
            {
                double checkMarge = solde - x - (temp2 * intDecouvert);
                if (checkMarge < decouvertMax)
                {
                    MessageBox.Show("fonds insuffisants");
                    return;
                }
                else
                {
                    solde -= x + (temp2 * intDecouvert);
                    return;
                }
            }
            else
            {
                solde -= x;
            }
        }

        internal void addComptes(object x)
        {
            throw new NotImplementedException();
        }
    }
}