﻿//Written By Frank
using System;
using System.Collections.Generic;

namespace SystemBanquier
{
    public class Agent : Directeur
    {
        private DateTime dateEmbauche { get; set; }
        private List<Client> listClients;
        private string idAgence { get; set; }
        private static int cnt;

        public Agent()
        {
        }

        public Agent(DateTime dateEmbache, List<Client> listClients, string idAgence, string empId, double revenu,
           string Nom, string Prenom, string Adresse)
           : base(empId, revenu, Nom, Prenom)
        {
            this.listClients = listClients;
            this.dateEmbauche = dateEmbauche;
            this.idAgence = idAgence;
            cnt++;
        }

        public List<Client> LISTCLIENTS
        {
            get { return this.listClients; }
        }

        public void addClient(Client x)
        {
            listClients.Add(x);
        }
    }
}