﻿namespace SystemBanquier
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.tabPageClients = new System.Windows.Forms.TabPage();
            this.butShowInfoAcc = new System.Windows.Forms.Button();
            this.lbInfoClient1 = new System.Windows.Forms.ListBox();
            this.radbutCompteNonRem = new System.Windows.Forms.RadioButton();
            this.radbutCompteRem = new System.Windows.Forms.RadioButton();
            this.txtDepotCompte = new System.Windows.Forms.TextBox();
            this.butCreerCompte = new System.Windows.Forms.Button();
            this.txtMontantOp = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblDepotCompte = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.butRetraitOp = new System.Windows.Forms.Button();
            this.butDepotOp = new System.Windows.Forms.Button();
            this.lbInfoClient = new System.Windows.Forms.ListBox();
            this.groupBoxCompte = new System.Windows.Forms.GroupBox();
            this.butDeconnect = new System.Windows.Forms.Button();
            this.butConnect = new System.Windows.Forms.Button();
            this.txtIDClientCompte = new System.Windows.Forms.TextBox();
            this.lblIDClientCompte = new System.Windows.Forms.Label();
            this.tabPageEmployes = new System.Windows.Forms.TabPage();
            this.EffEmp = new System.Windows.Forms.Button();
            this.cbAgentClient = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.CreerEmp = new System.Windows.Forms.Button();
            this.lbClients = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.butCreerClient = new System.Windows.Forms.Button();
            this.txtIdClient = new System.Windows.Forms.TextBox();
            this.txtPrenomClient = new System.Windows.Forms.TextBox();
            this.txtNomClient = new System.Windows.Forms.TextBox();
            this.txtSalaireEmp = new System.Windows.Forms.TextBox();
            this.txtAdressEmp = new System.Windows.Forms.TextBox();
            this.txtPrenomEmp = new System.Windows.Forms.TextBox();
            this.txtNomEmp = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.butTransfEmp = new System.Windows.Forms.Button();
            this.butModEmp = new System.Windows.Forms.Button();
            this.lbEmp = new System.Windows.Forms.ListBox();
            this.cbSuccEmp = new System.Windows.Forms.ComboBox();
            this.dtpDateEmp = new System.Windows.Forms.DateTimePicker();
            this.lblSalaireEmp = new System.Windows.Forms.Label();
            this.lblDateEmp = new System.Windows.Forms.Label();
            this.lblSuccEmp = new System.Windows.Forms.Label();
            this.lblIDEmpEmp = new System.Windows.Forms.Label();
            this.lblPrenomEmp = new System.Windows.Forms.Label();
            this.lblNomEmp = new System.Windows.Forms.Label();
            this.tabPageSuccursales = new System.Windows.Forms.TabPage();
            this.EffSucc = new System.Windows.Forms.Button();
            this.ModSucc = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.CreerSucc = new System.Windows.Forms.Button();
            this.lbSucc = new System.Windows.Forms.ListBox();
            this.cbBanqueSucc = new System.Windows.Forms.ComboBox();
            this.txtAdresseSucc = new System.Windows.Forms.TextBox();
            this.txtNomSucc = new System.Windows.Forms.TextBox();
            this.lblAdresseSucc = new System.Windows.Forms.Label();
            this.lblNomSucc = new System.Windows.Forms.Label();
            this.tabPageBanques = new System.Windows.Forms.TabPage();
            this.lbInfoBank = new System.Windows.Forms.ListBox();
            this.showInfoBanque = new System.Windows.Forms.Button();
            this.EffBanque = new System.Windows.Forms.Button();
            this.ModBanque = new System.Windows.Forms.Button();
            this.lbBanque = new System.Windows.Forms.ListBox();
            this.butCreerBanque = new System.Windows.Forms.Button();
            this.cbDirecteurBanque = new System.Windows.Forms.ComboBox();
            this.txtAdresseBanque = new System.Windows.Forms.TextBox();
            this.txtNomBanque = new System.Windows.Forms.TextBox();
            this.lblDirecteurBanque = new System.Windows.Forms.Label();
            this.lblAdresseBanque = new System.Windows.Forms.Label();
            this.lblNomBanque = new System.Windows.Forms.Label();
            this.tabPageDirecteurs = new System.Windows.Forms.TabPage();
            this.butEffDir = new System.Windows.Forms.Button();
            this.butModDir = new System.Windows.Forms.Button();
            this.butCreerDir = new System.Windows.Forms.Button();
            this.lbDir = new System.Windows.Forms.ListBox();
            this.txtSalaireDir = new System.Windows.Forms.TextBox();
            this.txtPrenomDir = new System.Windows.Forms.TextBox();
            this.txtNomDir = new System.Windows.Forms.TextBox();
            this.lblSalaireDir = new System.Windows.Forms.Label();
            this.lblPrenomDir = new System.Windows.Forms.Label();
            this.lblNomDir = new System.Windows.Forms.Label();
            this.tabControlSystemeBanquier = new System.Windows.Forms.TabControl();
            this.Banque = new System.Windows.Forms.Label();
            this.cbSuccursaleClient = new System.Windows.Forms.ComboBox();
            this.tabPageClients.SuspendLayout();
            this.groupBoxCompte.SuspendLayout();
            this.tabPageEmployes.SuspendLayout();
            this.tabPageSuccursales.SuspendLayout();
            this.tabPageBanques.SuspendLayout();
            this.tabPageDirecteurs.SuspendLayout();
            this.tabControlSystemeBanquier.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPageClients
            // 
            this.tabPageClients.BackColor = System.Drawing.Color.Aquamarine;
            this.tabPageClients.Controls.Add(this.butShowInfoAcc);
            this.tabPageClients.Controls.Add(this.lbInfoClient1);
            this.tabPageClients.Controls.Add(this.radbutCompteNonRem);
            this.tabPageClients.Controls.Add(this.radbutCompteRem);
            this.tabPageClients.Controls.Add(this.txtDepotCompte);
            this.tabPageClients.Controls.Add(this.butCreerCompte);
            this.tabPageClients.Controls.Add(this.txtMontantOp);
            this.tabPageClients.Controls.Add(this.label5);
            this.tabPageClients.Controls.Add(this.lblDepotCompte);
            this.tabPageClients.Controls.Add(this.label6);
            this.tabPageClients.Controls.Add(this.butRetraitOp);
            this.tabPageClients.Controls.Add(this.butDepotOp);
            this.tabPageClients.Controls.Add(this.lbInfoClient);
            this.tabPageClients.Controls.Add(this.groupBoxCompte);
            this.tabPageClients.Location = new System.Drawing.Point(4, 22);
            this.tabPageClients.Name = "tabPageClients";
            this.tabPageClients.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageClients.Size = new System.Drawing.Size(930, 485);
            this.tabPageClients.TabIndex = 4;
            this.tabPageClients.Text = "Clients";
            // 
            // butShowInfoAcc
            // 
            this.butShowInfoAcc.Location = new System.Drawing.Point(729, 77);
            this.butShowInfoAcc.Name = "butShowInfoAcc";
            this.butShowInfoAcc.Size = new System.Drawing.Size(129, 75);
            this.butShowInfoAcc.TabIndex = 33;
            this.butShowInfoAcc.Text = "Afficher les info reliées au comptes";
            this.butShowInfoAcc.UseVisualStyleBackColor = true;
            this.butShowInfoAcc.Click += new System.EventHandler(this.butShowInfoAcc_Click);
            // 
            // lbInfoClient1
            // 
            this.lbInfoClient1.FormattingEnabled = true;
            this.lbInfoClient1.Location = new System.Drawing.Point(268, 16);
            this.lbInfoClient1.Name = "lbInfoClient1";
            this.lbInfoClient1.Size = new System.Drawing.Size(120, 186);
            this.lbInfoClient1.TabIndex = 32;
            // 
            // radbutCompteNonRem
            // 
            this.radbutCompteNonRem.AutoSize = true;
            this.radbutCompteNonRem.Checked = true;
            this.radbutCompteNonRem.Location = new System.Drawing.Point(124, 318);
            this.radbutCompteNonRem.Name = "radbutCompteNonRem";
            this.radbutCompteNonRem.Size = new System.Drawing.Size(105, 17);
            this.radbutCompteNonRem.TabIndex = 31;
            this.radbutCompteNonRem.TabStop = true;
            this.radbutCompteNonRem.Text = "Compte non-rém.";
            this.radbutCompteNonRem.UseVisualStyleBackColor = true;
            // 
            // radbutCompteRem
            // 
            this.radbutCompteRem.AutoSize = true;
            this.radbutCompteRem.Location = new System.Drawing.Point(124, 295);
            this.radbutCompteRem.Name = "radbutCompteRem";
            this.radbutCompteRem.Size = new System.Drawing.Size(108, 17);
            this.radbutCompteRem.TabIndex = 30;
            this.radbutCompteRem.Text = "Compte rémunéré";
            this.radbutCompteRem.UseVisualStyleBackColor = true;
            // 
            // txtDepotCompte
            // 
            this.txtDepotCompte.Location = new System.Drawing.Point(124, 266);
            this.txtDepotCompte.Name = "txtDepotCompte";
            this.txtDepotCompte.Size = new System.Drawing.Size(100, 20);
            this.txtDepotCompte.TabIndex = 21;
            this.txtDepotCompte.Text = "100";
            // 
            // butCreerCompte
            // 
            this.butCreerCompte.Enabled = false;
            this.butCreerCompte.Location = new System.Drawing.Point(40, 295);
            this.butCreerCompte.Name = "butCreerCompte";
            this.butCreerCompte.Size = new System.Drawing.Size(75, 23);
            this.butCreerCompte.TabIndex = 18;
            this.butCreerCompte.Text = "Créer";
            this.butCreerCompte.UseVisualStyleBackColor = true;
            this.butCreerCompte.Click += new System.EventHandler(this.butCreerCompte_Click);
            // 
            // txtMontantOp
            // 
            this.txtMontantOp.Location = new System.Drawing.Point(533, 221);
            this.txtMontantOp.Name = "txtMontantOp";
            this.txtMontantOp.Size = new System.Drawing.Size(100, 20);
            this.txtMontantOp.TabIndex = 24;
            this.txtMontantOp.Text = "200";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(440, 224);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Montant:";
            // 
            // lblDepotCompte
            // 
            this.lblDepotCompte.AutoSize = true;
            this.lblDepotCompte.Location = new System.Drawing.Point(54, 269);
            this.lblDepotCompte.Name = "lblDepotCompte";
            this.lblDepotCompte.Size = new System.Drawing.Size(39, 13);
            this.lblDepotCompte.TabIndex = 17;
            this.lblDepotCompte.Text = "Dépôt:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(296, 266);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "$Solde$";
            // 
            // butRetraitOp
            // 
            this.butRetraitOp.Enabled = false;
            this.butRetraitOp.Location = new System.Drawing.Point(558, 269);
            this.butRetraitOp.Name = "butRetraitOp";
            this.butRetraitOp.Size = new System.Drawing.Size(75, 23);
            this.butRetraitOp.TabIndex = 20;
            this.butRetraitOp.Text = "Retrait";
            this.butRetraitOp.UseVisualStyleBackColor = true;
            this.butRetraitOp.Click += new System.EventHandler(this.butRetraitOp_Click_1);
            // 
            // butDepotOp
            // 
            this.butDepotOp.Enabled = false;
            this.butDepotOp.Location = new System.Drawing.Point(443, 269);
            this.butDepotOp.Name = "butDepotOp";
            this.butDepotOp.Size = new System.Drawing.Size(75, 23);
            this.butDepotOp.TabIndex = 19;
            this.butDepotOp.Text = "Dépôt";
            this.butDepotOp.UseVisualStyleBackColor = true;
            this.butDepotOp.Click += new System.EventHandler(this.butDepotOp_Click_1);
            // 
            // lbInfoClient
            // 
            this.lbInfoClient.FormattingEnabled = true;
            this.lbInfoClient.Location = new System.Drawing.Point(417, 16);
            this.lbInfoClient.Name = "lbInfoClient";
            this.lbInfoClient.Size = new System.Drawing.Size(306, 186);
            this.lbInfoClient.TabIndex = 18;
            this.lbInfoClient.SelectedIndexChanged += new System.EventHandler(this.lbInfoClient_SelectedIndexChanged);
            // 
            // groupBoxCompte
            // 
            this.groupBoxCompte.BackColor = System.Drawing.Color.DimGray;
            this.groupBoxCompte.Controls.Add(this.butDeconnect);
            this.groupBoxCompte.Controls.Add(this.butConnect);
            this.groupBoxCompte.Controls.Add(this.txtIDClientCompte);
            this.groupBoxCompte.Controls.Add(this.lblIDClientCompte);
            this.groupBoxCompte.Location = new System.Drawing.Point(6, 6);
            this.groupBoxCompte.Name = "groupBoxCompte";
            this.groupBoxCompte.Size = new System.Drawing.Size(218, 196);
            this.groupBoxCompte.TabIndex = 14;
            this.groupBoxCompte.TabStop = false;
            this.groupBoxCompte.Text = "Compte";
            // 
            // butDeconnect
            // 
            this.butDeconnect.Location = new System.Drawing.Point(87, 136);
            this.butDeconnect.Name = "butDeconnect";
            this.butDeconnect.Size = new System.Drawing.Size(75, 23);
            this.butDeconnect.TabIndex = 22;
            this.butDeconnect.Text = "LogOut";
            this.butDeconnect.UseVisualStyleBackColor = true;
            this.butDeconnect.Click += new System.EventHandler(this.butDeconnect_Click);
            // 
            // butConnect
            // 
            this.butConnect.Location = new System.Drawing.Point(87, 107);
            this.butConnect.Name = "butConnect";
            this.butConnect.Size = new System.Drawing.Size(75, 23);
            this.butConnect.TabIndex = 21;
            this.butConnect.Text = "Connexion";
            this.butConnect.UseVisualStyleBackColor = true;
            this.butConnect.Click += new System.EventHandler(this.butConnect_Click);
            // 
            // txtIDClientCompte
            // 
            this.txtIDClientCompte.Location = new System.Drawing.Point(87, 71);
            this.txtIDClientCompte.Name = "txtIDClientCompte";
            this.txtIDClientCompte.Size = new System.Drawing.Size(100, 20);
            this.txtIDClientCompte.TabIndex = 20;
            this.txtIDClientCompte.Text = "dra1";
            this.txtIDClientCompte.TextChanged += new System.EventHandler(this.txtIDClientCompte_TextChanged);
            // 
            // lblIDClientCompte
            // 
            this.lblIDClientCompte.AutoSize = true;
            this.lblIDClientCompte.Location = new System.Drawing.Point(17, 74);
            this.lblIDClientCompte.Name = "lblIDClientCompte";
            this.lblIDClientCompte.Size = new System.Drawing.Size(50, 13);
            this.lblIDClientCompte.TabIndex = 16;
            this.lblIDClientCompte.Text = "ID Client:";
            // 
            // tabPageEmployes
            // 
            this.tabPageEmployes.BackColor = System.Drawing.Color.Gold;
            this.tabPageEmployes.Controls.Add(this.EffEmp);
            this.tabPageEmployes.Controls.Add(this.cbAgentClient);
            this.tabPageEmployes.Controls.Add(this.label8);
            this.tabPageEmployes.Controls.Add(this.CreerEmp);
            this.tabPageEmployes.Controls.Add(this.lbClients);
            this.tabPageEmployes.Controls.Add(this.button1);
            this.tabPageEmployes.Controls.Add(this.button2);
            this.tabPageEmployes.Controls.Add(this.butCreerClient);
            this.tabPageEmployes.Controls.Add(this.cbSuccursaleClient);
            this.tabPageEmployes.Controls.Add(this.txtIdClient);
            this.tabPageEmployes.Controls.Add(this.txtPrenomClient);
            this.tabPageEmployes.Controls.Add(this.txtNomClient);
            this.tabPageEmployes.Controls.Add(this.txtSalaireEmp);
            this.tabPageEmployes.Controls.Add(this.txtAdressEmp);
            this.tabPageEmployes.Controls.Add(this.txtPrenomEmp);
            this.tabPageEmployes.Controls.Add(this.txtNomEmp);
            this.tabPageEmployes.Controls.Add(this.Banque);
            this.tabPageEmployes.Controls.Add(this.label2);
            this.tabPageEmployes.Controls.Add(this.label3);
            this.tabPageEmployes.Controls.Add(this.label4);
            this.tabPageEmployes.Controls.Add(this.butTransfEmp);
            this.tabPageEmployes.Controls.Add(this.butModEmp);
            this.tabPageEmployes.Controls.Add(this.lbEmp);
            this.tabPageEmployes.Controls.Add(this.cbSuccEmp);
            this.tabPageEmployes.Controls.Add(this.dtpDateEmp);
            this.tabPageEmployes.Controls.Add(this.lblSalaireEmp);
            this.tabPageEmployes.Controls.Add(this.lblDateEmp);
            this.tabPageEmployes.Controls.Add(this.lblSuccEmp);
            this.tabPageEmployes.Controls.Add(this.lblIDEmpEmp);
            this.tabPageEmployes.Controls.Add(this.lblPrenomEmp);
            this.tabPageEmployes.Controls.Add(this.lblNomEmp);
            this.tabPageEmployes.Location = new System.Drawing.Point(4, 22);
            this.tabPageEmployes.Name = "tabPageEmployes";
            this.tabPageEmployes.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEmployes.Size = new System.Drawing.Size(930, 485);
            this.tabPageEmployes.TabIndex = 3;
            this.tabPageEmployes.Text = "Employés";
            // 
            // EffEmp
            // 
            this.EffEmp.Location = new System.Drawing.Point(181, 220);
            this.EffEmp.Name = "EffEmp";
            this.EffEmp.Size = new System.Drawing.Size(75, 23);
            this.EffEmp.TabIndex = 34;
            this.EffEmp.Text = "Effacer";
            this.EffEmp.UseVisualStyleBackColor = true;
            this.EffEmp.Click += new System.EventHandler(this.EffEmp_Click);
            // 
            // cbAgentClient
            // 
            this.cbAgentClient.FormattingEnabled = true;
            this.cbAgentClient.Location = new System.Drawing.Point(586, 20);
            this.cbAgentClient.Name = "cbAgentClient";
            this.cbAgentClient.Size = new System.Drawing.Size(121, 21);
            this.cbAgentClient.TabIndex = 33;
            this.cbAgentClient.SelectedIndexChanged += new System.EventHandler(this.cbAgentClient_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(484, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Agent";
            // 
            // CreerEmp
            // 
            this.CreerEmp.Enabled = false;
            this.CreerEmp.Location = new System.Drawing.Point(19, 220);
            this.CreerEmp.Name = "CreerEmp";
            this.CreerEmp.Size = new System.Drawing.Size(75, 23);
            this.CreerEmp.TabIndex = 31;
            this.CreerEmp.Text = "Créer";
            this.CreerEmp.UseVisualStyleBackColor = true;
            this.CreerEmp.Click += new System.EventHandler(this.CreerEmp_Click);
            // 
            // lbClients
            // 
            this.lbClients.FormattingEnabled = true;
            this.lbClients.Location = new System.Drawing.Point(730, 20);
            this.lbClients.Name = "lbClients";
            this.lbClients.Size = new System.Drawing.Size(157, 173);
            this.lbClients.TabIndex = 30;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(648, 220);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "Effacer";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(567, 220);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 26;
            this.button2.Text = "Modifier";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // butCreerClient
            // 
            this.butCreerClient.Enabled = false;
            this.butCreerClient.Location = new System.Drawing.Point(486, 220);
            this.butCreerClient.Name = "butCreerClient";
            this.butCreerClient.Size = new System.Drawing.Size(75, 23);
            this.butCreerClient.TabIndex = 25;
            this.butCreerClient.Text = "Créer";
            this.butCreerClient.UseVisualStyleBackColor = true;
            this.butCreerClient.Click += new System.EventHandler(this.butCreerClient_Click);
            // 
            // txtIdClient
            // 
            this.txtIdClient.Location = new System.Drawing.Point(586, 116);
            this.txtIdClient.Name = "txtIdClient";
            this.txtIdClient.Size = new System.Drawing.Size(121, 20);
            this.txtIdClient.TabIndex = 23;
            this.txtIdClient.Text = "dra1";
            // 
            // txtPrenomClient
            // 
            this.txtPrenomClient.Location = new System.Drawing.Point(586, 84);
            this.txtPrenomClient.Name = "txtPrenomClient";
            this.txtPrenomClient.Size = new System.Drawing.Size(121, 20);
            this.txtPrenomClient.TabIndex = 22;
            this.txtPrenomClient.Text = "Daenerys";
            // 
            // txtNomClient
            // 
            this.txtNomClient.Location = new System.Drawing.Point(586, 56);
            this.txtNomClient.Name = "txtNomClient";
            this.txtNomClient.Size = new System.Drawing.Size(121, 20);
            this.txtNomClient.TabIndex = 21;
            this.txtNomClient.Text = "Targaryen";
            // 
            // txtSalaireEmp
            // 
            this.txtSalaireEmp.Location = new System.Drawing.Point(139, 165);
            this.txtSalaireEmp.Name = "txtSalaireEmp";
            this.txtSalaireEmp.Size = new System.Drawing.Size(133, 20);
            this.txtSalaireEmp.TabIndex = 9;
            this.txtSalaireEmp.Text = "48000";
            // 
            // txtAdressEmp
            // 
            this.txtAdressEmp.Location = new System.Drawing.Point(139, 139);
            this.txtAdressEmp.Name = "txtAdressEmp";
            this.txtAdressEmp.Size = new System.Drawing.Size(133, 20);
            this.txtAdressEmp.TabIndex = 8;
            this.txtAdressEmp.Text = "654 ChestBrre";
            // 
            // txtPrenomEmp
            // 
            this.txtPrenomEmp.Location = new System.Drawing.Point(139, 109);
            this.txtPrenomEmp.Name = "txtPrenomEmp";
            this.txtPrenomEmp.Size = new System.Drawing.Size(133, 20);
            this.txtPrenomEmp.TabIndex = 7;
            this.txtPrenomEmp.Text = "Johny";
            // 
            // txtNomEmp
            // 
            this.txtNomEmp.Location = new System.Drawing.Point(139, 78);
            this.txtNomEmp.Name = "txtNomEmp";
            this.txtNomEmp.Size = new System.Drawing.Size(133, 20);
            this.txtNomEmp.TabIndex = 6;
            this.txtNomEmp.Text = "Bravo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(484, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "ID Client:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(484, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Prénom:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(484, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Nom:";
            // 
            // butTransfEmp
            // 
            this.butTransfEmp.Location = new System.Drawing.Point(262, 220);
            this.butTransfEmp.Name = "butTransfEmp";
            this.butTransfEmp.Size = new System.Drawing.Size(75, 23);
            this.butTransfEmp.TabIndex = 16;
            this.butTransfEmp.Text = "Transférer";
            this.butTransfEmp.UseVisualStyleBackColor = true;
            this.butTransfEmp.Click += new System.EventHandler(this.butTransfEmp_Click_1);
            // 
            // butModEmp
            // 
            this.butModEmp.Location = new System.Drawing.Point(100, 220);
            this.butModEmp.Name = "butModEmp";
            this.butModEmp.Size = new System.Drawing.Size(75, 23);
            this.butModEmp.TabIndex = 14;
            this.butModEmp.Text = "Modifier";
            this.butModEmp.UseVisualStyleBackColor = true;
            this.butModEmp.Click += new System.EventHandler(this.butModEmp_Click_1);
            // 
            // lbEmp
            // 
            this.lbEmp.FormattingEnabled = true;
            this.lbEmp.Location = new System.Drawing.Point(303, 20);
            this.lbEmp.Name = "lbEmp";
            this.lbEmp.Size = new System.Drawing.Size(144, 173);
            this.lbEmp.TabIndex = 12;
            // 
            // cbSuccEmp
            // 
            this.cbSuccEmp.FormattingEnabled = true;
            this.cbSuccEmp.Location = new System.Drawing.Point(139, 22);
            this.cbSuccEmp.Name = "cbSuccEmp";
            this.cbSuccEmp.Size = new System.Drawing.Size(133, 21);
            this.cbSuccEmp.TabIndex = 11;
            this.cbSuccEmp.SelectedIndexChanged += new System.EventHandler(this.cbSuccEmp_SelectedIndexChanged);
            // 
            // dtpDateEmp
            // 
            this.dtpDateEmp.Location = new System.Drawing.Point(139, 50);
            this.dtpDateEmp.Name = "dtpDateEmp";
            this.dtpDateEmp.Size = new System.Drawing.Size(133, 20);
            this.dtpDateEmp.TabIndex = 10;
            // 
            // lblSalaireEmp
            // 
            this.lblSalaireEmp.AutoSize = true;
            this.lblSalaireEmp.Location = new System.Drawing.Point(16, 168);
            this.lblSalaireEmp.Name = "lblSalaireEmp";
            this.lblSalaireEmp.Size = new System.Drawing.Size(42, 13);
            this.lblSalaireEmp.TabIndex = 5;
            this.lblSalaireEmp.Text = "Salaire:";
            // 
            // lblDateEmp
            // 
            this.lblDateEmp.AutoSize = true;
            this.lblDateEmp.Location = new System.Drawing.Point(16, 56);
            this.lblDateEmp.Name = "lblDateEmp";
            this.lblDateEmp.Size = new System.Drawing.Size(94, 13);
            this.lblDateEmp.TabIndex = 4;
            this.lblDateEmp.Text = "Date d\'embauche:";
            // 
            // lblSuccEmp
            // 
            this.lblSuccEmp.AutoSize = true;
            this.lblSuccEmp.Location = new System.Drawing.Point(16, 25);
            this.lblSuccEmp.Name = "lblSuccEmp";
            this.lblSuccEmp.Size = new System.Drawing.Size(63, 13);
            this.lblSuccEmp.TabIndex = 3;
            this.lblSuccEmp.Text = "Succursale:";
            // 
            // lblIDEmpEmp
            // 
            this.lblIDEmpEmp.AutoSize = true;
            this.lblIDEmpEmp.Location = new System.Drawing.Point(16, 142);
            this.lblIDEmpEmp.Name = "lblIDEmpEmp";
            this.lblIDEmpEmp.Size = new System.Drawing.Size(45, 13);
            this.lblIDEmpEmp.TabIndex = 2;
            this.lblIDEmpEmp.Text = "Adresse";
            // 
            // lblPrenomEmp
            // 
            this.lblPrenomEmp.AutoSize = true;
            this.lblPrenomEmp.Location = new System.Drawing.Point(16, 112);
            this.lblPrenomEmp.Name = "lblPrenomEmp";
            this.lblPrenomEmp.Size = new System.Drawing.Size(46, 13);
            this.lblPrenomEmp.TabIndex = 1;
            this.lblPrenomEmp.Text = "Prénom:";
            // 
            // lblNomEmp
            // 
            this.lblNomEmp.AutoSize = true;
            this.lblNomEmp.Location = new System.Drawing.Point(16, 81);
            this.lblNomEmp.Name = "lblNomEmp";
            this.lblNomEmp.Size = new System.Drawing.Size(32, 13);
            this.lblNomEmp.TabIndex = 0;
            this.lblNomEmp.Text = "Nom:";
            // 
            // tabPageSuccursales
            // 
            this.tabPageSuccursales.BackColor = System.Drawing.Color.Lime;
            this.tabPageSuccursales.Controls.Add(this.EffSucc);
            this.tabPageSuccursales.Controls.Add(this.ModSucc);
            this.tabPageSuccursales.Controls.Add(this.label7);
            this.tabPageSuccursales.Controls.Add(this.CreerSucc);
            this.tabPageSuccursales.Controls.Add(this.lbSucc);
            this.tabPageSuccursales.Controls.Add(this.cbBanqueSucc);
            this.tabPageSuccursales.Controls.Add(this.txtAdresseSucc);
            this.tabPageSuccursales.Controls.Add(this.txtNomSucc);
            this.tabPageSuccursales.Controls.Add(this.lblAdresseSucc);
            this.tabPageSuccursales.Controls.Add(this.lblNomSucc);
            this.tabPageSuccursales.Location = new System.Drawing.Point(4, 22);
            this.tabPageSuccursales.Name = "tabPageSuccursales";
            this.tabPageSuccursales.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSuccursales.Size = new System.Drawing.Size(930, 485);
            this.tabPageSuccursales.TabIndex = 2;
            this.tabPageSuccursales.Text = "Succursales";
            // 
            // EffSucc
            // 
            this.EffSucc.Location = new System.Drawing.Point(175, 166);
            this.EffSucc.Name = "EffSucc";
            this.EffSucc.Size = new System.Drawing.Size(75, 23);
            this.EffSucc.TabIndex = 15;
            this.EffSucc.Text = "Effacer";
            this.EffSucc.UseVisualStyleBackColor = true;
            this.EffSucc.Click += new System.EventHandler(this.EffSucc_Click);
            // 
            // ModSucc
            // 
            this.ModSucc.Location = new System.Drawing.Point(94, 166);
            this.ModSucc.Name = "ModSucc";
            this.ModSucc.Size = new System.Drawing.Size(75, 23);
            this.ModSucc.TabIndex = 14;
            this.ModSucc.Text = "Modifier";
            this.ModSucc.UseVisualStyleBackColor = true;
            this.ModSucc.Click += new System.EventHandler(this.ModSucc_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Banque";
            // 
            // CreerSucc
            // 
            this.CreerSucc.Enabled = false;
            this.CreerSucc.Location = new System.Drawing.Point(13, 166);
            this.CreerSucc.Name = "CreerSucc";
            this.CreerSucc.Size = new System.Drawing.Size(75, 23);
            this.CreerSucc.TabIndex = 12;
            this.CreerSucc.Text = "Créer";
            this.CreerSucc.UseVisualStyleBackColor = true;
            this.CreerSucc.Click += new System.EventHandler(this.CreerSucc_Click);
            // 
            // lbSucc
            // 
            this.lbSucc.FormattingEnabled = true;
            this.lbSucc.Location = new System.Drawing.Point(256, 16);
            this.lbSucc.Name = "lbSucc";
            this.lbSucc.Size = new System.Drawing.Size(154, 173);
            this.lbSucc.TabIndex = 8;
            // 
            // cbBanqueSucc
            // 
            this.cbBanqueSucc.FormattingEnabled = true;
            this.cbBanqueSucc.Location = new System.Drawing.Point(94, 122);
            this.cbBanqueSucc.Name = "cbBanqueSucc";
            this.cbBanqueSucc.Size = new System.Drawing.Size(100, 21);
            this.cbBanqueSucc.TabIndex = 7;
            this.cbBanqueSucc.SelectedIndexChanged += new System.EventHandler(this.cbBanqueSucc_SelectedIndexChanged);
            // 
            // txtAdresseSucc
            // 
            this.txtAdresseSucc.Location = new System.Drawing.Point(94, 69);
            this.txtAdresseSucc.Name = "txtAdresseSucc";
            this.txtAdresseSucc.Size = new System.Drawing.Size(100, 20);
            this.txtAdresseSucc.TabIndex = 6;
            this.txtAdresseSucc.Text = "1234 boul telleville";
            // 
            // txtNomSucc
            // 
            this.txtNomSucc.Location = new System.Drawing.Point(94, 16);
            this.txtNomSucc.Name = "txtNomSucc";
            this.txtNomSucc.Size = new System.Drawing.Size(100, 20);
            this.txtNomSucc.TabIndex = 5;
            this.txtNomSucc.Text = "De Telleville";
            // 
            // lblAdresseSucc
            // 
            this.lblAdresseSucc.AutoSize = true;
            this.lblAdresseSucc.Location = new System.Drawing.Point(16, 69);
            this.lblAdresseSucc.Name = "lblAdresseSucc";
            this.lblAdresseSucc.Size = new System.Drawing.Size(48, 13);
            this.lblAdresseSucc.TabIndex = 2;
            this.lblAdresseSucc.Text = "Adresse:";
            // 
            // lblNomSucc
            // 
            this.lblNomSucc.AutoSize = true;
            this.lblNomSucc.Location = new System.Drawing.Point(16, 19);
            this.lblNomSucc.Name = "lblNomSucc";
            this.lblNomSucc.Size = new System.Drawing.Size(32, 13);
            this.lblNomSucc.TabIndex = 1;
            this.lblNomSucc.Text = "Nom:";
            // 
            // tabPageBanques
            // 
            this.tabPageBanques.BackColor = System.Drawing.Color.DodgerBlue;
            this.tabPageBanques.Controls.Add(this.lbInfoBank);
            this.tabPageBanques.Controls.Add(this.showInfoBanque);
            this.tabPageBanques.Controls.Add(this.EffBanque);
            this.tabPageBanques.Controls.Add(this.ModBanque);
            this.tabPageBanques.Controls.Add(this.lbBanque);
            this.tabPageBanques.Controls.Add(this.butCreerBanque);
            this.tabPageBanques.Controls.Add(this.cbDirecteurBanque);
            this.tabPageBanques.Controls.Add(this.txtAdresseBanque);
            this.tabPageBanques.Controls.Add(this.txtNomBanque);
            this.tabPageBanques.Controls.Add(this.lblDirecteurBanque);
            this.tabPageBanques.Controls.Add(this.lblAdresseBanque);
            this.tabPageBanques.Controls.Add(this.lblNomBanque);
            this.tabPageBanques.Location = new System.Drawing.Point(4, 22);
            this.tabPageBanques.Name = "tabPageBanques";
            this.tabPageBanques.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBanques.Size = new System.Drawing.Size(930, 485);
            this.tabPageBanques.TabIndex = 1;
            this.tabPageBanques.Text = "Banques";
            // 
            // lbInfoBank
            // 
            this.lbInfoBank.AccessibleName = "";
            this.lbInfoBank.FormattingEnabled = true;
            this.lbInfoBank.Location = new System.Drawing.Point(299, 175);
            this.lbInfoBank.Name = "lbInfoBank";
            this.lbInfoBank.Size = new System.Drawing.Size(170, 277);
            this.lbInfoBank.TabIndex = 13;
            // 
            // showInfoBanque
            // 
            this.showInfoBanque.Enabled = false;
            this.showInfoBanque.Location = new System.Drawing.Point(81, 236);
            this.showInfoBanque.Name = "showInfoBanque";
            this.showInfoBanque.Size = new System.Drawing.Size(131, 123);
            this.showInfoBanque.TabIndex = 12;
            this.showInfoBanque.Text = "Voir Info";
            this.showInfoBanque.UseVisualStyleBackColor = true;
            this.showInfoBanque.Click += new System.EventHandler(this.showInfoBanque_Click);
            // 
            // EffBanque
            // 
            this.EffBanque.Location = new System.Drawing.Point(199, 143);
            this.EffBanque.Name = "EffBanque";
            this.EffBanque.Size = new System.Drawing.Size(75, 23);
            this.EffBanque.TabIndex = 11;
            this.EffBanque.Text = "Effacer";
            this.EffBanque.UseVisualStyleBackColor = true;
            this.EffBanque.Click += new System.EventHandler(this.EffBanque_Click);
            // 
            // ModBanque
            // 
            this.ModBanque.Location = new System.Drawing.Point(108, 143);
            this.ModBanque.Name = "ModBanque";
            this.ModBanque.Size = new System.Drawing.Size(75, 23);
            this.ModBanque.TabIndex = 10;
            this.ModBanque.Text = "Modifier";
            this.ModBanque.UseVisualStyleBackColor = true;
            this.ModBanque.Click += new System.EventHandler(this.ModBanque_Click);
            // 
            // lbBanque
            // 
            this.lbBanque.FormattingEnabled = true;
            this.lbBanque.Location = new System.Drawing.Point(299, 22);
            this.lbBanque.Name = "lbBanque";
            this.lbBanque.Size = new System.Drawing.Size(170, 147);
            this.lbBanque.TabIndex = 9;
            this.lbBanque.Tag = "";
            this.lbBanque.SelectedIndexChanged += new System.EventHandler(this.lbBanque_SelectedIndexChanged);
            // 
            // butCreerBanque
            // 
            this.butCreerBanque.Enabled = false;
            this.butCreerBanque.Location = new System.Drawing.Point(17, 143);
            this.butCreerBanque.Name = "butCreerBanque";
            this.butCreerBanque.Size = new System.Drawing.Size(75, 23);
            this.butCreerBanque.TabIndex = 6;
            this.butCreerBanque.Text = "Créer";
            this.butCreerBanque.UseVisualStyleBackColor = true;
            this.butCreerBanque.Click += new System.EventHandler(this.butCreerBanque_Click);
            // 
            // cbDirecteurBanque
            // 
            this.cbDirecteurBanque.FormattingEnabled = true;
            this.cbDirecteurBanque.Location = new System.Drawing.Point(120, 101);
            this.cbDirecteurBanque.Name = "cbDirecteurBanque";
            this.cbDirecteurBanque.Size = new System.Drawing.Size(121, 21);
            this.cbDirecteurBanque.TabIndex = 5;
            this.cbDirecteurBanque.SelectedIndexChanged += new System.EventHandler(this.cbDirecteurBanque_SelectedIndexChanged);
            // 
            // txtAdresseBanque
            // 
            this.txtAdresseBanque.Location = new System.Drawing.Point(120, 61);
            this.txtAdresseBanque.Name = "txtAdresseBanque";
            this.txtAdresseBanque.Size = new System.Drawing.Size(121, 20);
            this.txtAdresseBanque.TabIndex = 4;
            this.txtAdresseBanque.Text = "8888 Rue sansnom";
            // 
            // txtNomBanque
            // 
            this.txtNomBanque.Location = new System.Drawing.Point(120, 22);
            this.txtNomBanque.Name = "txtNomBanque";
            this.txtNomBanque.Size = new System.Drawing.Size(121, 20);
            this.txtNomBanque.TabIndex = 3;
            this.txtNomBanque.Text = "Du Quebec";
            // 
            // lblDirecteurBanque
            // 
            this.lblDirecteurBanque.AutoSize = true;
            this.lblDirecteurBanque.Location = new System.Drawing.Point(30, 104);
            this.lblDirecteurBanque.Name = "lblDirecteurBanque";
            this.lblDirecteurBanque.Size = new System.Drawing.Size(53, 13);
            this.lblDirecteurBanque.TabIndex = 2;
            this.lblDirecteurBanque.Text = "Directeur:";
            // 
            // lblAdresseBanque
            // 
            this.lblAdresseBanque.AutoSize = true;
            this.lblAdresseBanque.Location = new System.Drawing.Point(30, 64);
            this.lblAdresseBanque.Name = "lblAdresseBanque";
            this.lblAdresseBanque.Size = new System.Drawing.Size(48, 13);
            this.lblAdresseBanque.TabIndex = 1;
            this.lblAdresseBanque.Text = "Adresse:";
            // 
            // lblNomBanque
            // 
            this.lblNomBanque.AutoSize = true;
            this.lblNomBanque.Location = new System.Drawing.Point(30, 25);
            this.lblNomBanque.Name = "lblNomBanque";
            this.lblNomBanque.Size = new System.Drawing.Size(32, 13);
            this.lblNomBanque.TabIndex = 0;
            this.lblNomBanque.Text = "Nom:";
            // 
            // tabPageDirecteurs
            // 
            this.tabPageDirecteurs.BackColor = System.Drawing.Color.DarkOrange;
            this.tabPageDirecteurs.Controls.Add(this.butEffDir);
            this.tabPageDirecteurs.Controls.Add(this.butModDir);
            this.tabPageDirecteurs.Controls.Add(this.butCreerDir);
            this.tabPageDirecteurs.Controls.Add(this.lbDir);
            this.tabPageDirecteurs.Controls.Add(this.txtSalaireDir);
            this.tabPageDirecteurs.Controls.Add(this.txtPrenomDir);
            this.tabPageDirecteurs.Controls.Add(this.txtNomDir);
            this.tabPageDirecteurs.Controls.Add(this.lblSalaireDir);
            this.tabPageDirecteurs.Controls.Add(this.lblPrenomDir);
            this.tabPageDirecteurs.Controls.Add(this.lblNomDir);
            this.tabPageDirecteurs.Location = new System.Drawing.Point(4, 22);
            this.tabPageDirecteurs.Name = "tabPageDirecteurs";
            this.tabPageDirecteurs.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDirecteurs.Size = new System.Drawing.Size(930, 485);
            this.tabPageDirecteurs.TabIndex = 0;
            this.tabPageDirecteurs.Text = "Directeurs";
            // 
            // butEffDir
            // 
            this.butEffDir.Location = new System.Drawing.Point(181, 166);
            this.butEffDir.Name = "butEffDir";
            this.butEffDir.Size = new System.Drawing.Size(75, 23);
            this.butEffDir.TabIndex = 11;
            this.butEffDir.Text = "Effacer";
            this.butEffDir.UseVisualStyleBackColor = true;
            this.butEffDir.Click += new System.EventHandler(this.butEffDir_Click);
            // 
            // butModDir
            // 
            this.butModDir.Location = new System.Drawing.Point(100, 166);
            this.butModDir.Name = "butModDir";
            this.butModDir.Size = new System.Drawing.Size(75, 23);
            this.butModDir.TabIndex = 10;
            this.butModDir.Text = "Modifier";
            this.butModDir.UseVisualStyleBackColor = true;
            this.butModDir.Click += new System.EventHandler(this.butModDir_Click);
            // 
            // butCreerDir
            // 
            this.butCreerDir.Location = new System.Drawing.Point(19, 166);
            this.butCreerDir.Name = "butCreerDir";
            this.butCreerDir.Size = new System.Drawing.Size(75, 23);
            this.butCreerDir.TabIndex = 9;
            this.butCreerDir.Text = "Créer";
            this.butCreerDir.UseVisualStyleBackColor = true;
            this.butCreerDir.Click += new System.EventHandler(this.butCreerDir_Click);
            // 
            // lbDir
            // 
            this.lbDir.FormattingEnabled = true;
            this.lbDir.Location = new System.Drawing.Point(262, 18);
            this.lbDir.Name = "lbDir";
            this.lbDir.Size = new System.Drawing.Size(167, 173);
            this.lbDir.TabIndex = 8;
            this.lbDir.SelectedIndexChanged += new System.EventHandler(this.lbDir_SelectedIndexChanged);
            // 
            // txtSalaireDir
            // 
            this.txtSalaireDir.Location = new System.Drawing.Point(122, 71);
            this.txtSalaireDir.Name = "txtSalaireDir";
            this.txtSalaireDir.Size = new System.Drawing.Size(100, 20);
            this.txtSalaireDir.TabIndex = 7;
            this.txtSalaireDir.Text = "275000";
            // 
            // txtPrenomDir
            // 
            this.txtPrenomDir.Location = new System.Drawing.Point(122, 45);
            this.txtPrenomDir.Name = "txtPrenomDir";
            this.txtPrenomDir.Size = new System.Drawing.Size(100, 20);
            this.txtPrenomDir.TabIndex = 5;
            this.txtPrenomDir.Text = "Boss";
            // 
            // txtNomDir
            // 
            this.txtNomDir.Location = new System.Drawing.Point(122, 18);
            this.txtNomDir.Name = "txtNomDir";
            this.txtNomDir.Size = new System.Drawing.Size(100, 20);
            this.txtNomDir.TabIndex = 4;
            this.txtNomDir.Text = "Le";
            // 
            // lblSalaireDir
            // 
            this.lblSalaireDir.AutoSize = true;
            this.lblSalaireDir.Location = new System.Drawing.Point(16, 74);
            this.lblSalaireDir.Name = "lblSalaireDir";
            this.lblSalaireDir.Size = new System.Drawing.Size(42, 13);
            this.lblSalaireDir.TabIndex = 3;
            this.lblSalaireDir.Text = "Salaire:";
            // 
            // lblPrenomDir
            // 
            this.lblPrenomDir.AutoSize = true;
            this.lblPrenomDir.Location = new System.Drawing.Point(16, 48);
            this.lblPrenomDir.Name = "lblPrenomDir";
            this.lblPrenomDir.Size = new System.Drawing.Size(46, 13);
            this.lblPrenomDir.TabIndex = 1;
            this.lblPrenomDir.Text = "Prénom:";
            // 
            // lblNomDir
            // 
            this.lblNomDir.AutoSize = true;
            this.lblNomDir.Location = new System.Drawing.Point(16, 21);
            this.lblNomDir.Name = "lblNomDir";
            this.lblNomDir.Size = new System.Drawing.Size(32, 13);
            this.lblNomDir.TabIndex = 0;
            this.lblNomDir.Text = "Nom:";
            // 
            // tabControlSystemeBanquier
            // 
            this.tabControlSystemeBanquier.Controls.Add(this.tabPageDirecteurs);
            this.tabControlSystemeBanquier.Controls.Add(this.tabPageBanques);
            this.tabControlSystemeBanquier.Controls.Add(this.tabPageSuccursales);
            this.tabControlSystemeBanquier.Controls.Add(this.tabPageEmployes);
            this.tabControlSystemeBanquier.Controls.Add(this.tabPageClients);
            this.tabControlSystemeBanquier.Location = new System.Drawing.Point(3, 12);
            this.tabControlSystemeBanquier.Name = "tabControlSystemeBanquier";
            this.tabControlSystemeBanquier.SelectedIndex = 0;
            this.tabControlSystemeBanquier.Size = new System.Drawing.Size(938, 511);
            this.tabControlSystemeBanquier.TabIndex = 1;
            // 
            // Banque
            // 
            this.Banque.AutoSize = true;
            this.Banque.Location = new System.Drawing.Point(484, 151);
            this.Banque.Name = "Banque";
            this.Banque.Size = new System.Drawing.Size(63, 13);
            this.Banque.TabIndex = 20;
            this.Banque.Text = "Succursale:";
            // 
            // cbSuccursaleClient
            // 
            this.cbSuccursaleClient.FormattingEnabled = true;
            this.cbSuccursaleClient.Location = new System.Drawing.Point(586, 148);
            this.cbSuccursaleClient.Name = "cbSuccursaleClient";
            this.cbSuccursaleClient.Size = new System.Drawing.Size(121, 21);
            this.cbSuccursaleClient.TabIndex = 24;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 725);
            this.Controls.Add(this.tabControlSystemeBanquier);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabPageClients.ResumeLayout(false);
            this.tabPageClients.PerformLayout();
            this.groupBoxCompte.ResumeLayout(false);
            this.groupBoxCompte.PerformLayout();
            this.tabPageEmployes.ResumeLayout(false);
            this.tabPageEmployes.PerformLayout();
            this.tabPageSuccursales.ResumeLayout(false);
            this.tabPageSuccursales.PerformLayout();
            this.tabPageBanques.ResumeLayout(false);
            this.tabPageBanques.PerformLayout();
            this.tabPageDirecteurs.ResumeLayout(false);
            this.tabPageDirecteurs.PerformLayout();
            this.tabControlSystemeBanquier.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TabPage tabPageClients;
        private System.Windows.Forms.TextBox txtMontantOp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button butRetraitOp;
        private System.Windows.Forms.Button butDepotOp;
        private System.Windows.Forms.ListBox lbInfoClient;
        private System.Windows.Forms.GroupBox groupBoxCompte;
        private System.Windows.Forms.TextBox txtDepotCompte;
        private System.Windows.Forms.TextBox txtIDClientCompte;
        private System.Windows.Forms.Button butCreerCompte;
        private System.Windows.Forms.Label lblIDClientCompte;
        private System.Windows.Forms.Label lblDepotCompte;
        private System.Windows.Forms.TabPage tabPageEmployes;
        private System.Windows.Forms.ListBox lbClients;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button butCreerClient;
        private System.Windows.Forms.TextBox txtIdClient;
        private System.Windows.Forms.TextBox txtPrenomClient;
        private System.Windows.Forms.TextBox txtNomClient;
        private System.Windows.Forms.TextBox txtSalaireEmp;
        private System.Windows.Forms.TextBox txtAdressEmp;
        private System.Windows.Forms.TextBox txtPrenomEmp;
        private System.Windows.Forms.TextBox txtNomEmp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button butTransfEmp;
        private System.Windows.Forms.Button butModEmp;
        private System.Windows.Forms.ListBox lbEmp;
        private System.Windows.Forms.ComboBox cbSuccEmp;
        private System.Windows.Forms.DateTimePicker dtpDateEmp;
        private System.Windows.Forms.Label lblSalaireEmp;
        private System.Windows.Forms.Label lblDateEmp;
        private System.Windows.Forms.Label lblSuccEmp;
        private System.Windows.Forms.Label lblIDEmpEmp;
        private System.Windows.Forms.Label lblPrenomEmp;
        private System.Windows.Forms.Label lblNomEmp;
        private System.Windows.Forms.TabPage tabPageSuccursales;
        private System.Windows.Forms.ListBox lbSucc;
        private System.Windows.Forms.ComboBox cbBanqueSucc;
        private System.Windows.Forms.TextBox txtAdresseSucc;
        private System.Windows.Forms.TextBox txtNomSucc;
        private System.Windows.Forms.Label lblAdresseSucc;
        private System.Windows.Forms.Label lblNomSucc;
        private System.Windows.Forms.TabPage tabPageBanques;
        private System.Windows.Forms.ListBox lbBanque;
        private System.Windows.Forms.Button butCreerBanque;
        private System.Windows.Forms.ComboBox cbDirecteurBanque;
        private System.Windows.Forms.TextBox txtAdresseBanque;
        private System.Windows.Forms.TextBox txtNomBanque;
        private System.Windows.Forms.Label lblDirecteurBanque;
        private System.Windows.Forms.Label lblAdresseBanque;
        private System.Windows.Forms.Label lblNomBanque;
        private System.Windows.Forms.TabPage tabPageDirecteurs;
        private System.Windows.Forms.Button butEffDir;
        private System.Windows.Forms.Button butModDir;
        private System.Windows.Forms.Button butCreerDir;
        private System.Windows.Forms.ListBox lbDir;
        private System.Windows.Forms.TextBox txtSalaireDir;
        private System.Windows.Forms.TextBox txtPrenomDir;
        private System.Windows.Forms.TextBox txtNomDir;
        private System.Windows.Forms.Label lblSalaireDir;
        private System.Windows.Forms.Label lblPrenomDir;
        private System.Windows.Forms.Label lblNomDir;
        private System.Windows.Forms.TabControl tabControlSystemeBanquier;
        private System.Windows.Forms.RadioButton radbutCompteNonRem;
        private System.Windows.Forms.RadioButton radbutCompteRem;
        private System.Windows.Forms.Button CreerSucc;
        private System.Windows.Forms.Button CreerEmp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbAgentClient;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button EffBanque;
        private System.Windows.Forms.Button ModBanque;
        private System.Windows.Forms.Button EffSucc;
        private System.Windows.Forms.Button ModSucc;
        private System.Windows.Forms.Button EffEmp;
        private System.Windows.Forms.ListBox lbInfoClient1;
        private System.Windows.Forms.Button butConnect;
        private System.Windows.Forms.ListBox lbInfoBank;
        private System.Windows.Forms.Button showInfoBanque;
        private System.Windows.Forms.Button butDeconnect;
        private System.Windows.Forms.Button butShowInfoAcc;
        private System.Windows.Forms.ComboBox cbSuccursaleClient;
        private System.Windows.Forms.Label Banque;
    }
}

