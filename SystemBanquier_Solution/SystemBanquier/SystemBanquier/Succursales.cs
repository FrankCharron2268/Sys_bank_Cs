﻿//Written By Frank
using System.Collections.Generic;

namespace SystemBanquier
{
    public class Succursales : Banques
    {
        private string idSucc;
        private string nomSucc { get; set; }
        private List<Agent> listAgent { get; set; }
        private static int cnt = 0;

        public Succursales() ///update
        {
        }

        public Succursales(string idSucc, string nomSucc, List<Agent> listAgent, string nomBanque, string adresse,
            Directeur directeur, double capitalGlobal, List<Succursales> listSuccursale, List<string> listnomsucc)
            : base(nomBanque, adresse, directeur, capitalGlobal, listSuccursale, listnomsucc)
        {
            this.idSucc = toString(); ///update
            this.nomSucc = nomSucc;
            this.listAgent = listAgent;

            cnt++;
        }

        public override string toString()
        {
            string result = "";
            result += nomSucc;
            return result;
        }

        public List<Agent> LISTAGENT
        {
            get { return this.listAgent; }
        }
        public string succID() ///update
        {
            string ID = "SC" + cnt;
            return ID;
        }

        public string IDSUCC
        {
            get { return this.idSucc; }
        }

        public string NOMSUCC
        {
            get { return this.nomSucc; }
        }

        public string ADDRESSE
        {
            get { return this.ADDRESSE; }
        }

        public void addEmp(Agent x)
        {
            listAgent.Add(x);
        }

        public void modSucc(string x)
        {
            ADRESSE = x;
        }
    }
}